public class Probability {
    double value = 0.0;
    public Probability(double value){
        this.value = value;
    }
    public Probability and(Probability otherProbability){
        double combinedProbability = this.value * otherProbability.value;
        return new Probability(combinedProbability);
    }
    public Probability or(Probability otherProbability){
        double combinedProbability = this.value + otherProbability.value;
        return new Probability(combinedProbability);
    }
    public Probability inverse(){
        double inverseProbability = 1- this.value;
        return new Probability(inverseProbability);
    }
    public Probability xor(Probability otherProbability){
        Probability firstProbability = this;
        double xorProbability = firstProbability.value + otherProbability.value - 2*firstProbability.value* otherProbability.value;
        return new Probability(xorProbability);
    }
    public Probability bayesianProbability(Probability otherProbability){
        Probability firstProbability = this;
        if(otherProbability.value != 0) {
            double bayesianProbability = (((otherProbability.or(firstProbability)).and(firstProbability)).value) / otherProbability.value;
            return new Probability(bayesianProbability);
        }
        return new Probability(0.0);
    } //((P(B) or P(A)) and P(A)) / P(B)
    public Boolean isEqualTo(Probability otherProbability){
        return this.value == otherProbability.value;
    }
}
